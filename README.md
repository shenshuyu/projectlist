# ProjectList 项目链接

## brpc demo：
* https://gitlab.com/shensy/brpc?nav_source=navbar

## p2p chat
* https://gitlab.com/shenshuyu/p2pchat

## udp反向消息代理器
* https://gitlab.com/shenshuyu/devicemessageproxy

## cap代理器
* https://gitlab.com/shenshuyu/cap_proxy

## 网页视频播放插件
* https://gitlab.com/shenshuyu/WebPlugin

## rtsp 取流客户端vs2013工程
* https://gitlab.com/shenshuyu/rtspClient

